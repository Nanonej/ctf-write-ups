# HTB | [Lame](https://app.hackthebox.com/machines/1)

**Author**: Nanonej

**Status:** [Rooted]

**Tags:** #Linux #Injection #CMSExploit #SMB #Samba #usermap_script

**Vulnerabilities:** Samba's "username map script" configuration option.
+ Security misconfigurations
+ Remote code execution

**Rating:** Easy

## Overview

Lame is a beginner level machine, requiring only one exploit to obtain root access. It was the first
machine published on Hack The Box and was often the first machine for new users prior to its
retirement.

---

## Reconnaissance

### NMAP
```console
$ nmap -sC -A -Pn -oA lame.nmap 10.10.10.3
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-21 13:06 EST
Nmap scan report for 10.10.10.3
Host is up (0.040s latency).
Not shown: 996 filtered ports
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to 10.10.14.15
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp  open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey:
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 2h35m23s, deviation: 3h32m11s, median: 5m20s
| smb-os-discovery:
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name:
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-11-21T13:12:06-05:00
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 56.10 seconds
```

We discover several ports open with FTP (vsftpd 2.3.4), SSH (OpenSSH 4.7) and SMB (Samba 3.0.20-Debian) services. \
Possible user : lame.

FTP Anonymous is empty. \
CVE 2011-2523 found for vsftpd 2.3.4.

No exploit found for OpenSSH 4.7.

CVE 2007-2447 found for Samba 3.0.20.

## Vulnerability Analysis

The vsftpd download from the master site (vsftpd-2.3.4.tar.gz) appeared to contain a backdoor. \
In response to a :) smiley face in the FTP username, a TCP callback shell is attempted.

In Samba versions 3.0.20 through 3.0.25rc3 when using the non-default "username map script" configuration option. By specifying a username containing shell meta characters, attackers can execute arbitrary commands. No authentication is needed to exploit this vulnerability since this option is used to map usernames prior to authentication!

## Exploitation

Possible backdoor in vsftpd 2.3.4, doesn't work.

For manual exploitation see references.

### Metasploit
`exploit/multi/samba/usermap_script`

### User Flag
Can be found in `"/home/makis/user.txt"`

### Root Flag
Can be found in `"/root/root.txt"`

---

## References

- https://scarybeastsecurity.blogspot.com/2011/07/alert-vsftpd-download-backdoored.html
- https://westoahu.hawaii.edu/cyber/forensics-weekly-executive-summmaries/8424-2/
- https://amriunix.com/post/cve-2007-2447-samba-usermap-script/
