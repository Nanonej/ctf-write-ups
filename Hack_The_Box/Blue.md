# HTB | [Blue](https://app.hackthebox.com/machines/Blue)

**Author**: Nanonej

**Status:** [Rooted]

**Tags:** #Windows #PatchManagement #SMB #MS17-010 #EternalBlue

**Vulnerabilities:** MS17-010 EternalBlue.
+ SMBv1 CVE, EternalBlue unpatched
+ Remote code execution

**Rating:** Easy

## Overview

Blue, while possibly the most simple machine on Hack The Box, demonstrates the severity of the
EternalBlue exploit, which has been used in multiple large-scale ransomware and crypto-mining
attacks since it was leaked publicly.

---

## Reconnaissance

### NMAP
```console
Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-15 12:35 EST
NSE: Loaded 153 scripts for scanning.
NSE: Script Pre-scanning.
NSE: Starting runlevel 1 (of 3) scan.
Initiating NSE at 12:35
Completed NSE at 12:35, 0.00s elapsed
NSE: Starting runlevel 2 (of 3) scan.
Initiating NSE at 12:35
Completed NSE at 12:35, 0.00s elapsed
NSE: Starting runlevel 3 (of 3) scan.
Initiating NSE at 12:35
Completed NSE at 12:35, 0.00s elapsed
Initiating Ping Scan at 12:35
Scanning 10.10.10.40 [2 ports]
Completed Ping Scan at 12:35, 0.04s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 12:35
Completed Parallel DNS resolution of 1 host. at 12:35, 0.03s elapsed
Initiating Connect Scan at 12:35
Scanning 10.10.10.40 [1000 ports]
Discovered open port 139/tcp on 10.10.10.40
Discovered open port 445/tcp on 10.10.10.40
Discovered open port 135/tcp on 10.10.10.40
Discovered open port 49153/tcp on 10.10.10.40
Discovered open port 49155/tcp on 10.10.10.40
Discovered open port 49156/tcp on 10.10.10.40
Discovered open port 49154/tcp on 10.10.10.40
Discovered open port 49157/tcp on 10.10.10.40
Discovered open port 49152/tcp on 10.10.10.40
Completed Connect Scan at 12:35, 0.58s elapsed (1000 total ports)
Initiating Service scan at 12:35
Scanning 9 services on 10.10.10.40
Service scan Timing: About 44.44% done; ETC: 12:37 (0:01:09 remaining)
Completed Service scan at 12:36, 59.33s elapsed (9 services on 1 host)
NSE: Script scanning 10.10.10.40.
NSE: Starting runlevel 1 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 10.89s elapsed
NSE: Starting runlevel 2 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 0.00s elapsed
NSE: Starting runlevel 3 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 0.00s elapsed
Nmap scan report for 10.10.10.40
Host is up, received conn-refused (0.036s latency).
Scanned at 2021-11-15 12:35:44 EST for 71s
Not shown: 991 closed ports
Reason: 991 conn-refused
PORT      STATE SERVICE      REASON  VERSION
135/tcp   open  msrpc        syn-ack Microsoft Windows RPC
139/tcp   open  netbios-ssn  syn-ack Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds syn-ack Windows 7 Professional 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
49152/tcp open  msrpc        syn-ack Microsoft Windows RPC
49153/tcp open  msrpc        syn-ack Microsoft Windows RPC
49154/tcp open  msrpc        syn-ack Microsoft Windows RPC
49155/tcp open  msrpc        syn-ack Microsoft Windows RPC
49156/tcp open  msrpc        syn-ack Microsoft Windows RPC
49157/tcp open  msrpc        syn-ack Microsoft Windows RPC
Service Info: Host: HARIS-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 5m03s, deviation: 3s, median: 5m01s
| p2p-conficker:
|   Checking for Conficker.C or higher...
|   Check 1 (port 52925/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 12383/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 19006/udp): CLEAN (Failed to receive data)
|   Check 4 (port 64989/udp): CLEAN (Timeout)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb-os-discovery:
|   OS: Windows 7 Professional 7601 Service Pack 1 (Windows 7 Professional 6.1)
|   OS CPE: cpe:/o:microsoft:windows_7::sp1:professional
|   Computer name: haris-PC
|   NetBIOS computer name: HARIS-PC\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-11-15T17:41:51+00:00
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2021-11-15T17:41:48
|_  start_date: 2021-11-15T17:40:22

NSE: Script Post-scanning.
NSE: Starting runlevel 1 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 0.00s elapsed
NSE: Starting runlevel 2 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 0.00s elapsed
NSE: Starting runlevel 3 (of 3) scan.
Initiating NSE at 12:36
Completed NSE at 12:36, 0.00s elapsed
Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 71.35 seconds
```

We discover that the machine is running `SMB`, the OS is `Windows 7 Professional 7601 Service Pack 1 (Windows 7 Professional 6.1)` and an user `HARIS`. \
Nmap also tell us that the machine is vulnerable to `ms17-010`.

## Vulnerability Analysis

MS17-010 a.k.a. EternalBlue is known as the most enduring and damaging exploit of all time. \
EternalBlue is both the given name to a series of Microsoft software vulnerabilities and the exploit created by the NSA as a cyberattack tool, affecting anything that uses the SMBv1 protocol.

## Exploitation

For manual exploitation see references.

### Metasploit
exploit/windows/smb/ms17_010_eternalblue with LHOST=tun0

### User Flag
Can be found in `"C:\Users\haris\Desktop\user.txt"`

### Root Flag
Can be found in `"C:\Users\Administrator\Desktop\root.txt"`

---

## References

- [What is MS17-010 EternalBlue ?](https://www.avast.com/c-eternalblue)
- [EternalBlue manual exploit](https://root4loot.com/post/eternalblue_manual_exploit/)
- [MS17-010 Exploit Script](https://github.com/3ndG4me/AutoBlue-MS17-010)

---
