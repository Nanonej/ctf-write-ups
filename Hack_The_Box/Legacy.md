# HTB | [Legacy](https://app.hackthebox.com/machines/Legacy)

**Author**: Nanonej

**Status:** [Rooted]

**Tags:** #Windows #SMB #MS17-010

**Vulnerabilities:** MS17-010 EternalRomance/EternalSynergy/EternalChampion.
+ SMBv1 CVE, MS17-010 unpatched
+ Remote code execution

**Rating:** Easy

---

## Reconnaissance

### NMAP
```console
$ nmap -sC -A -Pn 10.10.10.4
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-20 09:27 EST
Nmap scan report for 10.10.10.4
Host is up (0.039s latency).
Not shown: 997 filtered ports
PORT     STATE  SERVICE       VERSION
139/tcp  open   netbios-ssn   Microsoft Windows netbios-ssn
445/tcp  open   microsoft-ds  Windows XP microsoft-ds
3389/tcp closed ms-wbt-server
Service Info: OSs: Windows, Windows XP; CPE: cpe:/o:microsoft:windows, cpe:/o:microsoft:windows_xp

Host script results:
|_clock-skew: mean: 5d01h02m39s, deviation: 1h24m51s, median: 5d00h02m39s
|_nbstat: NetBIOS name: LEGACY, NetBIOS user: <unknown>, NetBIOS MAC: 00:50:56:b9:54:ff (VMware)
| smb-os-discovery:
|   OS: Windows XP (Windows 2000 LAN Manager)
|   OS CPE: cpe:/o:microsoft:windows_xp::-
|   Computer name: legacy
|   NetBIOS computer name: LEGACY\x00
|   Workgroup: HTB\x00
|_  System time: 2021-11-25T18:30:05+02:00
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 64.20 seconds
```

OS: Windows XP (Windows 2000 LAN Manager) \
Potential user: legacy \
SMB service (ports 139 and 445)

```console
$ nmap -Pn --script "safe or smb-enum-*" -p 445 10.10.10.4
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2021-11-20 12:34 EST
Pre-scan script results:
|_hostmap-robtex: *TEMPORARILY DISABLED* due to changes in Robtex's API. See https://www.robtex.com/api/
|_http-robtex-shared-ns: *TEMPORARILY DISABLED* due to changes in Robtex's API. See https://www.robtex.com/api/
| targets-asn:
|_  targets-asn.asn is a mandatory parameter
Nmap scan report for 10.10.10.4
Host is up (0.037s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
|_smb-enum-services: ERROR: Script execution failed (use -d to debug)

Host script results:
|_clock-skew: mean: 5d01h02m40s, deviation: 1h24m50s, median: 5d00h02m40s
| dns-blacklist:
|   SPAM
|_    l2.apews.org - FAIL
|_fcrdns: FAIL (No PTR record)
|_msrpc-enum: NT_STATUS_ACCESS_DENIED
|_nbstat: NetBIOS name: LEGACY, NetBIOS user: <unknown>, NetBIOS MAC: 00:50:56:b9:19:de (VMware)
| smb-enum-shares:
|   note: ERROR: Enumerating shares failed, guessing at common ones (NT_STATUS_ACCESS_DENIED)
|   account_used: <blank>
|   \\10.10.10.4\ADMIN$:
|     warning: Couldn't get details for share: NT_STATUS_ACCESS_DENIED
|     Anonymous access: <none>
|   \\10.10.10.4\C$:
|     warning: Couldn't get details for share: NT_STATUS_ACCESS_DENIED
|     Anonymous access: <none>
|   \\10.10.10.4\IPC$:
|     warning: Couldn't get details for share: NT_STATUS_ACCESS_DENIED
|_    Anonymous access: READ
|_smb-mbenum: ERROR: Script execution failed (use -d to debug)
| smb-os-discovery:
|   OS: Windows XP (Windows 2000 LAN Manager)
|   OS CPE: cpe:/o:microsoft:windows_xp::-
|   Computer name: legacy
|   NetBIOS computer name: LEGACY\x00
|   Workgroup: HTB\x00
|_  System time: 2021-11-25T21:38:03+02:00
| smb-protocols:
|   dialects:
|_    NT LM 0.12 (SMBv1) [dangerous, but default]
| smb-security-mode:
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb-vuln-ms17-010:
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|
|     Disclosure date: 2017-03-14
|     References:
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_smb2-time: Protocol negotiation failed (SMB2)
| unusual-port:
|_  WARNING: this script depends on Nmap's service/version detection (-sV)

Post-scan script results:
| reverse-index:
|_  445/tcp: 10.10.10.4
Nmap done: 1 IP address (1 host up) scanned in 105.60 seconds
```

SMBv1 \
MS17-010 vuln discovered

## Vulnerability Analysis

MS17-010 a.k.a. EternalBlue is known as the most enduring and damaging exploit of all time. \
EternalBlue is both the given name to a series of Microsoft software vulnerabilities and the exploit created by the NSA as a cyberattack tool, affecting anything that uses the SMBv1 protocol.

Researchers from RiskSense have now discovered similar exploits, that can be used to target all versions of Windows. To maintain the existing category, the threats are called EternalChampion, EternalRomance and EternalSynergy.

## Exploitation

### Metasploit
EternalBlue don't work \
Let's check others ms17-010 exploits. \
Successfully logged in with EternalRomance/EternalSynergy/EternalChampion exploit as `Legacy` user.

### User Flag
Can be found in `"C:\Documents and Settings\john\Desktop\user.txt"`

### Root Flag
Can be found in `"C:\Documents and Settings\Administrator\Desktop\root.txt"`

---

## References

- [MS17-010 EternalSynergy / EternalRomance / EternalChampion](https://github.com/rapid7/metasploit-framework/pull/9473)
