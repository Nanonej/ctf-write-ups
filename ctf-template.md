# CTF Name | [link](https://localhost/)

**Author**: Nanonej

**Status:** [Rooted | User Pwnd | WIP]

**Tags:** #CTF #Security

**Vulnerabilities:** Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sodales feugiat elementum.
+ Security misconfigurations (fake)
+ Cross Site Scripting (XSS) (fake)
+ Remote code execution (fake)

**Rating:** Easy

## Overview

---

## Reconnaissance

### NMAP
```console

```

## Vulnerability Analysis

## Exploitation

### Flag

## Conclusion

---

## References
